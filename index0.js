const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

function createhttpserver() {
    const server = http.createServer((request, response) => {
        if (request.url === "/html") {
            response.writeHead(200, { "content-type": "text/html" });

            fs.readFile("/home/rama/httpdrill/data/data.html", "utf8", (err, data) => {
                if (err) {
                    console.log("Error:", err)
                } else {
                    console.log(data);
                    response.write(data);
                    response.end();
                }
            })
        }

        else if (request.url === "/json") {
            response.writeHead(200, { "content-type": "text/json" });

            fs.readFile("/home/rama/httpdrill/data/data.json", "utf8", (err, data) => {
                if (err) {
                    console.log("Error:", err)
                } else {
                    console.log(JSON.stringify(data))
                    response.write(JSON.stringify(data));
                    response.end();
                }
            })
        }

        else if (request.url === "/uuid") {
            response.writeHead(200, { "content-type": "text/application.json" })

            const new_uuid = uuidv4();

            const uuidObject = {
                uuid: new_uuid
            };

            response.write(JSON.stringify(uuidObject));

            response.end();
        }

        else if (request.url.startsWith('/status/')) {
            const statusCode = parseInt(request.url.split('/')[2]);
            response.writeHead(statusCode, { "content-type": "text/application.json" })
            const statusObject = {
                status: statusCode,
                message: http.STATUS_CODES[statusCode] || "unknown status code",
            };
            response.write(JSON.stringify(statusObject));
            response.end();
        }

        else if (request.url.startsWith('/delay/')) {
            const delay = parseInt(request.url.split('/')[2]);

            if (isNaN(delay) || delay <= 0) {
                response.writeHead(400, { "content-type": "text/application.json" });
                response.write(JSON.stringify({ erroe: "wrong daley time" }));
                response.end();
                return;
            }
            response.writeHead(200, { "content-type": "text/json" });

            setTimeout(() => {
                response.write(JSON.stringify({ message: "Success response after delay.", delay }));
                response.end();

            }, delay * 1000);

        }

        else {
            response.writeHead(404, { "content-type": "text/error" });
            response.write("404 Not Found");
            response.end();
        }

    });

    return server
}

let result = createhttpserver();
const port = 3000;
result.listen(port, () => {
    console.log("listening to the port no 3000 ")
})
